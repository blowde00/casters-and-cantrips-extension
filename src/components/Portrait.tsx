import Grid from '@mui/material/Grid';
import Image from 'mui-image';
import characterImage from '../assets/Emy_transparent.png';

function Portrait() {
  return (
    <Grid
      item
      xs={4.75}
      sx={{
        // backgroundColor: 'blue',
        maxHeight: '38.5rem',
        width: '17rem',
      }}>
      <Image src={`${characterImage}`} fit='contain' />
    </Grid>
  );
}

export default Portrait;
