import { useTheme } from '@mui/material/styles';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';

type Props = {
  name: string;
  score: number;
  modifier: number;
};

function AbilityScore(props: Props) {
  const theme = useTheme();

  return (
    <Grid
      item
      xs={4}
      key={props.name}
      sx={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Paper
        elevation={theme.palette.mode === 'dark' ? 1 : 4}
        sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          width: '100%',
          border: 'solid',
          borderWidth: 'thin',
        }}>
        <Typography sx={{ fontSize: '1.25rem' }}>{props.name}</Typography>
        <Typography sx={{ fontSize: '2.25rem', marginTop: '0.1rem' }}>
          {props.score}
        </Typography>
        <Typography
          sx={{
            fontSize: '1.1rem',
            width: '50%',
            borderStyle: 'solid',
            borderRadius: '50%',
            marginTop: '0.2rem',
            marginBottom: '0.5rem',
          }}>
          {props.modifier < 0 ? props.modifier : `+${props.modifier}`}
        </Typography>
      </Paper>
    </Grid>
  );
}

export default AbilityScore;
