import { useState, useContext } from 'react';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import SvgIcon from '@mui/material/SvgIcon';
import IconButton from '@mui/material/IconButton';
import Brightness7Icon from '@mui/icons-material/Brightness7';
import DarkModeIcon from '@mui/icons-material/DarkMode';
import ColorModeContext from '../context/ColorMode';
import Portrait from './Portrait';
import AbilityScore from './AbilityScore';
import response from '../data/Emy.json';

type equippedArmor = {
  type: string;
  name: string;
  armorClass: number;
};

type stats = {
  name: string;
  score: number;
  modifier: number;
};

function Character() {
  const theme = useTheme();
  const colorMode = useContext(ColorModeContext);
  const characterData = response.data;
  const [value, setValue] = useState(0);

  const stats: stats[] = [
    {
      name: 'STR',
      score: 11,
      modifier: 0,
    },
    {
      name: 'DEX',
      score: 8,
      modifier: -1,
    },
    {
      name: 'CON',
      score: 14,
      modifier: 2,
    },
    {
      name: 'INT',
      score: 13,
      modifier: 1,
    },
    {
      name: 'WIS',
      score: 16,
      modifier: 3,
    },
    {
      name: 'CHA',
      score: 14,
      modifier: 2,
    },
  ];

  const handleChange = (_event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };

  const determineClass = () => {
    return characterData.classes.length === 1
      ? characterData.classes[0].definition.name
      : 'Multiclass';
  };

  const determineLevel = () => {
    return characterData.classes.length === 1
      ? characterData.classes[0].level
      : 'Lots of Levels';
  };

  const determineSubclasses = () => {
    return characterData.classes.length === 1
      ? characterData.classes[0].subclassDefinition.name
      : 'Lots of Subclasses';
  };

  // const determineInitialConBonus = () => {
  //   return characterData.modifiers.race.findIndex(raceObj => raceObj.subType === 'constitution-score')
  // }

  const determineMaxHitPoints = () => {
    const conModifier = 2;
    const totalCharacterLevel = characterData.classes.reduce(
      (accumulator, currClass) => {
        return accumulator + currClass.level;
      },
      0,
    );
    return characterData.baseHitPoints + totalCharacterLevel * conModifier;
  };

  const determineCurrentHitPoints = () => {
    return determineMaxHitPoints() - characterData.removedHitPoints;
  };

  // let findInitConBonus = characterData.modifiers.race.findIndex(
  //   (raceObj) => raceObj.subType === 'constitution-score',
  // );

  let equippedArmor = characterData.inventory.reduce(
    (a: equippedArmor[], currentItem: any) => {
      if (
        currentItem.definition.filterType === 'Armor' &&
        currentItem.equipped === true
      ) {
        a.push({
          type: currentItem.definition.type,
          name: currentItem.definition.name,
          armorClass: currentItem.definition.armorClass,
        });
      }
      let firstShieldEncountered = false;
      let firstArmorEncountered = false;
      const filteredEquippedArmor: equippedArmor[] = a.filter((item) => {
        if (item.type === 'Shield') {
          if (firstShieldEncountered) {
            return false;
          }
          firstShieldEncountered = true;
        } else {
          if (firstArmorEncountered) {
            return false;
          }
          firstArmorEncountered = true;
        }
        return true;
      });
      return filteredEquippedArmor;
    },
    [],
  );

  const determineCurrentAc = () => {
    let dexModifier = -1;
    const baseArmorClass = equippedArmor.reduce((equippedArmorClass, item) => {
      return equippedArmorClass + item.armorClass;
    }, 0);
    return baseArmorClass + dexModifier;
  };

  return (
    <>
      <Box
        sx={{
          backgroundColor: theme.palette.mode === 'dark' ? '#1a1a1a' : '#fff',
          opacity: theme.palette.mode === 'dark' ? '0.7' : '0.6',
          height: '100%',
          width: '100%',
          padding: '1.5rem 2rem',
          position: 'absolute',
          zIndex: '-1',
        }}></Box>
      <Box
        sx={{
          height: '100%',
          width: '100%',
          padding: '1.5rem 2rem',
        }}>
        <Box sx={{ position: 'relative' }}>
          <IconButton
            sx={{
              position: 'absolute',
              padding: 0,
              marginTop: '-3.25%',
              marginLeft: '99%',
            }}
            onClick={colorMode.toggleColorMode}
            color='inherit'>
            {theme.palette.mode === 'dark' ? (
              <Brightness7Icon />
            ) : (
              <DarkModeIcon />
            )}
          </IconButton>
          <Tabs
            value={value}
            onChange={handleChange}
            variant='fullWidth'
            indicatorColor='primary'
            textColor='inherit'
            sx={{ marginBottom: '2rem', marginTop: '0.5rem' }}>
            <Tab label='Character'></Tab>
            <Tab label='Backstory'></Tab>
          </Tabs>
        </Box>
        <Grid
          container
          columnSpacing={{ xs: 4 }}
          sx={{
            // backgroundColor: 'red',
            height: '87%',
          }}>
          <Portrait />
          <Grid
            item
            xs={7.25}
            sx={{
              // backgroundColor: 'green',
              textAlign: 'start',
              paddingTop: '2rem',
            }}>
            <Typography sx={{ fontSize: '2rem' }}>
              {characterData.name}
            </Typography>
            <Box sx={{ display: 'flex', flexDirection: 'row' }}>
              <Typography sx={{ fontSize: '1.5rem', marginRight: '3rem' }}>
                {`${characterData.race.baseRaceName} ${determineClass()}`}
              </Typography>
              <Typography
                sx={{
                  fontSize: '1.5rem',
                }}>{`Level ${determineLevel()}`}</Typography>
            </Box>
            <Typography>{determineSubclasses()}</Typography>
            <Box>
              <Box
                sx={{
                  display: 'flex',
                  flexDirection: 'row',
                  // backgroundColor: 'aqua',
                }}>
                <Box
                  sx={{
                    position: 'relative',
                    // backgroundColor: 'red',
                    height: '10rem',
                    width: '10rem',
                  }}>
                  <SvgIcon
                    sx={{
                      position: 'absolute',
                      fontSize: '10rem',
                      marginLeft: '-1rem',
                    }}>
                    <svg
                      xmlns='http://www.w3.org/2000/svg'
                      height='48'
                      viewBox='0 -960 960 960'
                      width='48'
                      stroke='none'
                      strokeWidth={0}
                      fill={
                        theme.palette.mode === 'dark'
                          ? theme.palette.text.primary
                          : theme.palette.primary.main
                      }>
                      <path d='M480-134q-115-37-191.5-143.5T212-518v-206l268-100 268 100v206q0 134-76.5 240.5T480-134Zm0-24q107-34 176.5-135T726-518v-191l-246-92-246 92v191q0 124 69.5 225T480-158Zm0-321Z' />
                    </svg>
                  </SvgIcon>
                  <Typography
                    sx={{
                      fontSize: '1.5rem',
                      position: 'absolute',
                      width: '100%',
                      height: '100%',
                      textAlign: 'center',
                      paddingTop: '2rem',
                      marginLeft: '-1rem',
                    }}>
                    AC
                  </Typography>
                  <Typography
                    sx={{
                      fontSize: '3rem',
                      position: 'absolute',
                      width: '100%',
                      height: '100%',
                      textAlign: 'center',
                      paddingTop: '3.5rem',
                      marginLeft: '-1.1rem',
                    }}>
                    {determineCurrentAc()}
                  </Typography>
                </Box>
                <Box
                  sx={{
                    height: '10rem',
                    width: '8.2rem',
                    // backgroundColor: 'green',
                    display: 'flex',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                    marginLeft: '-0.5rem',
                  }}>
                  <Typography sx={{ fontSize: '1.3rem' }}>Health</Typography>
                  <Typography sx={{ fontSize: '2.5rem', marginTop: '-0.3rem' }}>
                    {determineCurrentHitPoints()}/{determineMaxHitPoints()}
                  </Typography>
                  <Typography sx={{ fontSize: '1rem' }}>
                    Temp HP: {characterData.temporaryHitPoints}
                  </Typography>
                </Box>
              </Box>
              <Divider
                sx={{
                  backgroundColor:
                    theme.palette.mode === 'dark'
                      ? 'primary.light'
                      : 'primary.main',
                  marginTop: '0.75rem',
                }}></Divider>
              <Grid
                container
                spacing={1.5}
                sx={{ textAlign: 'center', marginTop: '1rem' }}>
                {stats.map((stat) => (
                  <AbilityScore
                    name={stat.name}
                    score={stat.score}
                    modifier={stat.modifier}
                  />
                ))}
              </Grid>
            </Box>
          </Grid>
        </Grid>
      </Box>
    </>
  );
}

export default Character;
